from app import app, socketio
from flask import render_template
from flask_socketio import emit

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('show question')
def handle_show_question(data):
    partial = render_template('question.html', data=data)
    emit('render question', partial, broadcast=True)

This is a project to get live audience feedback using websockets!

Right now it is really rough around the edges, but I'm still working on it.

## Setup

`pipenv install`

The socket server is at `server/audience.py` so you'll need to run that. For fish shell it looks like this:
```
env FLASK_APP=audience.py FLASK_ENV=development pipenv run flask run
```
This websocket server also doubles as an HTTP server (still not sure how that all works). The HTTP server serves up the `index.html` that the audience will see. Partial HTML gets sent through websockets.

`sample_slides/` is what the presenter will be seeing. I usually just spin up a static python server
```
python -m http.server
```

## Next Steps

- [ ] **Allow recording of responses**
- [ ] Come up with a better project name
- [ ] Experiment with presenter interactions as GET/POST requests
- [ ] Style audience view